This is the simple application of this test :
https://nsl.cz/c-vs-php-vs-hhvm-vs-go-simple-benchmark/  

PHP CLI and GCC are needed to build and run tests.

All is done by running the `test.sh` script :
- compilation with different optimization level
- test execution

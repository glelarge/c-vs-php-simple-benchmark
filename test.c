#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
  // Default loop number.
  int nbLoop = 1000000000;

  // Read parameter.
  char *param;
  if (argc == 2) {
    nbLoop = atoi(strdup(argv[1]));
  }

  printf("Loop on %i\n", nbLoop);

  char buffer[500] = "test";

  long a = 0;
  int result = 0;
  char ch = '0';
  for (int i = 0; i < nbLoop; i++) {

    /*
    Basic implementation :
    - check odd/even using the modulo operator
    - concatenate to "test" string
    - compare with "test0" string

    The basic implementation is fast enough on small volumes, but optimizations are needed

    The function itoa() is often used to convert an integer to a string, but is not C standard then
    not available in GCC linux.
    Custom implementation can be very different on performance :
    http://www.strudel.org.uk/itoa/
    */
    // result = i % 2;
    // sprintf(buffer, "%s%i", "test", result);
    // char ibuffer[20];
    // itoa(result, ibuffer, 10);
    // strcat(buffer, ibuffer);

    // Bit check on odd/even.
    result = i & 2;
    // Implicit conversion to char to be concatenated to the string 'buffer'.
    ch = result + '0';
    strncat(buffer, &ch, 1);

    if (!strcmp(buffer, "test0")) {
      a += i;
    }

    // Reset the string termination char.
    buffer[4] = '\0';
  }
  printf("%ld\n", a);
  return 0;
}

<?php

// Default loop number.
$nbLoop = 1000000000;

// Read parameter.
if (isset($argv[1]) && intval($argv[1])) {
  $nbLoop = $argv[1];
}

print "Loop on $nbLoop\n";

$a = 0;
for ($i = 0; $i < $nbLoop; $i++) {

  // Basic implementation with modulo operator.
  // $result = $i % 2;

  // Bit check on odd/even.
  $result = $i & 2;

  if (!strcmp("test" . $result, "test0")) {
    $a += $i;
  }
}
print "$a\n";

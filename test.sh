#!/bin/bash

COMPILER="gcc"

NB_LOOP=1000

# Read parameter
if [ $# -eq 1 ]
then
  NB_LOOP=$1
fi

echo "Loop : ${NB_LOOP}"

echo "===== Compilation"

echo "-------"
echo "${COMPILER} -O0"
time ${COMPILER} test.c -O0 -o test_O0.exe

echo "-------"
echo "${COMPILER} -O1"
time ${COMPILER} test.c -O1 -o test_O1.exe

echo "-------"
echo "${COMPILER} -O2"
time ${COMPILER} test.c -O2 -o test_O2.exe

echo "-------"
echo "${COMPILER} -O3"
time ${COMPILER} test.c -O3 -o test_O3.exe

echo "-------"
echo "${COMPILER} -Os"
time ${COMPILER} test.c -Os -o test_Os.exe

echo "-------"
echo "${COMPILER} -Ofast"
time ${COMPILER} test.c -Ofast -o test_Ofast.exe

echo "===== Test"

echo "-------"
echo "${COMPILER} -O0"
time ./test_O0.exe ${NB_LOOP}

echo "-------"
echo "${COMPILER} -O1"
time ./test_O1.exe ${NB_LOOP}

echo "-------"
echo "${COMPILER} -O2"
time ./test_O2.exe ${NB_LOOP}

echo "-------"
echo "${COMPILER} -O3"
time ./test_O3.exe ${NB_LOOP}

echo "-------"
echo "${COMPILER} -Os"
time ./test_Os.exe ${NB_LOOP}

echo "-------"
echo "${COMPILER} -Ofast"
time ./test_Ofast.exe ${NB_LOOP}

echo "-------"
echo "PHP"
time php test.php ${NB_LOOP}

# Remove all binaries
rm *.exe
